import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import {AuthPage} from './App/containers/Auth';
import {MainPage} from './App/containers/Main';
import {appStore} from './Store/Store';

import './App/styles/style.less';

ReactDOM.render(
  <Provider store={appStore}>
      <div className="container">
          <BrowserRouter>
              <Switch>
                  <Route exact path="/" component={MainPage}/>
                  <Route path="/auth" component={AuthPage}/>
              </Switch>
          </BrowserRouter>
      </div>
  </Provider>,
  document.getElementById('app')
);
