export interface IStoreState {
    data?: number;
    loginStatus: boolean;
    loading: boolean;
    login?: string;
    organizations?: any;
    offices?: any;
    workers?: any;
    user?: any;
}