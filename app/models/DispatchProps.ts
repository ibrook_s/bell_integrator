import {Actions} from '../App/Actions/Actions';
import {OfficesActions} from '../App/Actions/Offices'
import {OrganizationsActions} from '../App/Actions/Organizations';
import {WorkersActions} from '../App/Actions/Workers';

export interface IDispatchProps {
    actions?: Actions,
    OrganizationsActions?: OrganizationsActions,
    OfficesActions?: OfficesActions,
    WorkersActions?: WorkersActions
}
