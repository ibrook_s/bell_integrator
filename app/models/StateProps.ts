//@todo Для каждого компонента нужен свой интерфейс пропсов.
export default interface IStateProps {
    loginStatus: boolean;
    loading: boolean;
    login: any;
    organizations: any;
    offices: any;
    workers: any;
}
