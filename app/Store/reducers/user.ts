import {IStoreState} from '../../models/StoreState';
import {ActionTypes, AsyncActionTypes} from '../../App/Actions/ActionsConsts';
import {IActionType} from '../Store';
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
        }
    }
};
export const user = (state: IStoreState = initialState.state, action: IActionType) => {
    switch (action.type) {
        case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loginStatus: true,
                loading: false,
                login: action.payload
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.FAIL}`:
            return {
                ...state,
                loading: false,
                loginStatus: false,
            };

        case ActionTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false,
            };
    }
    return state;
}