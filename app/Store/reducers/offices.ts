import {ActionTypes, AsyncActionTypes} from '../../App/Actions/ActionsConsts';
import {IActionType} from '../Store';
import {IStoreState} from '../../models/StoreState';
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
        }
    }
};
export const offices = (state: any = initialState.state, action: IActionType) => {
    switch(action.type) {
        case `${ActionTypes.LOAD_OFFICES}${AsyncActionTypes.SUCCESS}`:
            return action.payload;
        case `${ActionTypes.ADD_OFFICE}`:
            return [
                ...state,
                action.payload
            ];
        case `${ActionTypes.DELETE_OFFICE}`:
            console.log(state, action.payload);
            return state.filter((office: any) => {
                return office.id !== action.payload
            });
    }
    return state;
};