import {ActionTypes, AsyncActionTypes} from '../../App/Actions/ActionsConsts';
import {IActionType} from '../Store';
import {IStoreState} from '../../models/StoreState';
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
        }
    }
};
export const organizations = (state: any = initialState.state, action: IActionType) => {
    switch(action.type) {
        case `${ActionTypes.LOAD_ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`:
            return action.payload;
        case `${ActionTypes.ADD_ORGANIZATION}`:
            return [
                ...state,
                action.payload
            ];
        case `${ActionTypes.DELETE_ORGANIZATION}`:
            console.log(state, action.payload);
            return state.filter((organization: any) => {
                return organization.id !== action.payload
            });
    }
    return state;
};