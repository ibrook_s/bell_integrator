import {ActionTypes, AsyncActionTypes} from '../../App/Actions/ActionsConsts';
import {IActionType} from '../Store';
import {IStoreState} from '../../models/StoreState';
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
        }
    }
};
export const workers = (state: any = initialState.state, action: IActionType) => {
    switch(action.type) {
        case `${ActionTypes.LOAD_WORKERS}${AsyncActionTypes.SUCCESS}`:
            return action.payload;
        case `${ActionTypes.ADD_WORKER}`:
            return [
                ...state,
                action.payload
            ];
        case `${ActionTypes.UPDATE_WORKER}`:
            let newState = state;
            return newState.map((worker: any) => {
                console.log(worker);
                if (worker.id === action.payload.id && worker.offId === action.payload.offId) {
                    worker.name = action.payload.name;
                }
                return worker;
            });
        case `${ActionTypes.DELETE_WORKER}`:
            console.log(state, action.payload);
            return state.filter((worker: any) => {
                return worker.id !== action.payload
            });
    }
    return state;
};