import { Action, applyMiddleware, createStore } from 'redux';
import { combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { organizations } from './reducers/organizations';
import { offices } from './reducers/offices';
import { workers } from './reducers/workers';
import { user } from './reducers/user';
export interface IActionType extends Action {
  type: any;
  payload: any;
}
let reducers = combineReducers({
    organizations,
    offices,
    workers,
    user
});
const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

export {store as appStore};
