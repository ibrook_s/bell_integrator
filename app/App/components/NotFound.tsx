import * as React from 'react';
import {Link} from 'react-router-dom';
class Main extends React.Component {
    render() {
        return (
            <div>
                <h1>404</h1>
                <hr/>
                <Link to={`/`} >Вернуться на главную</Link>;
            </div>
        )
    }
}

export default Main;
