import * as React from 'react';
class AddNewOrganization extends React.Component<{addOrganization: any}, {orgName?: any, orgLogoUrl?: any}> {
    constructor () {
        super();
        this.state = {
            orgName: '',
            orgLogoUrl: '',
        }
    }
    changeOrgNameHandler = (e: any) => {
        this.setState({
            orgName: e.target.value
        })
    };
    changeOrgLogoUrlHandler = (e: any) => {
        this.setState({
            orgLogoUrl: e.target.value
        })
    };
    render() {
        return (
            <form>
                <div className="form-group">
                    <label>Название новой организации</label>
                    <input type="text" className="form-control" onChange={this.changeOrgNameHandler} placeholder="Название" />
                </div>
                <div className="form-group">
                    <label>Url логотипа новой организации</label>
                    <input type="text" className="form-control" onChange={this.changeOrgLogoUrlHandler} placeholder="url на логотип"/>
                </div>
                <div className="form-group">
                    <input type="button" className="btn btn-info" value="Добавить" onClick={() => this.props.addOrganization(this.state.orgName, this.state.orgLogoUrl)}/>
                </div>
            </form>
        )
    }
}
export default AddNewOrganization
