import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {Redirect} from 'react-router-dom';
import IStateProps from '../../models/StateProps';
import {IStoreState} from '../../models/StoreState'
import {Actions} from '../Actions/Actions';
import {OrganizationsActions} from '../Actions/Organizations';
import {OfficesActions} from '../Actions/Offices';
import {WorkersActions} from '../Actions/Workers';
import {IDispatchProps} from '../../models/DispatchProps';
import AddNewOrganization from './AddNewOrganization';
import AddNewWorker from './AddNewWorker';
import AddNewOffice from './AddNewOffice';
import Office from './Office';
import Organization from './Organization';
import Worker from './Worker';

import './tabs.less';
class Main extends React.Component<IDispatchProps & IStateProps, {
    currentOrganizationId: any, currentWorkersId: any, showAddOrganization: boolean, activeTab: Number }>{
    constructor() {
        super();
        this.state = {
            currentOrganizationId: 0,
            currentWorkersId: 0,
            showAddOrganization: false,
            activeTab: 0
        }
    }
    componentWillMount() {
        this.loadOrganizations();
    }
    changeOrganization = (id: any) => {
        this.setState({
            currentOrganizationId: id,
            currentWorkersId: 0,
            activeTab: 1
        });
        this.loadOffices(id);
    };
    changeWorkers = (id: any) => {
        this.setState({
            currentWorkersId: id,
            activeTab: 2
        });
        this.loadWorkers(id);
    };
    loadOrganizations = () => {
        const {actions} = this.props;
        actions.onLoadOrganizations();
        console.log('Загрузка организаций', this.props.organizations);
    };
    loadOffices = (orgId: Number) => {
        const {actions} = this.props;
        actions.onLoadOffices(orgId);
        console.log('Загрузка оффисов', this.props.offices);
    };
    loadWorkers = (offId: Number) => {
        const {actions} = this.props;
        actions.onLoadWorkers(offId);
        console.log('Загрузка работников', this.props.workers);
        console.log(this.props.loginStatus);
    };
    addOrganization = (name: any, logoUrl: any) => {
        const {OrganizationsActions} = this.props;
        OrganizationsActions.onAdd({
            id: this.props.organizations.length + 1,
            name: name,
            raiting: '1',
            logo: logoUrl,
            offices: []
        });
    };
    updateWorker = (id: Number, name: String) => {
        const { WorkersActions } = this.props;
        WorkersActions.onUpdate({
            id,
            offId: this.state.currentWorkersId,
            name,
        });
    };
    addWorker = (name: string) => {
        const { WorkersActions } = this.props;
        WorkersActions.onAdd({
            id: this.props.workers.length + 1,
            name,
            orgId: this.state.currentOrganizationId,
            offId: this.state.currentWorkersId
        });
    };
    showAddOrganization = () => {
        this.setState({
            showAddOrganization: !this.state.showAddOrganization
        });
    };
    deleteOrganization = (id: Number) => {
        const {OrganizationsActions} = this.props;
        OrganizationsActions.onDelete(id)
    };
    deleteOffice = (id: Number) => {
        const {OfficesActions} = this.props;
        OfficesActions.onDelete(id);
    };
    deleteWorker = (id: Number) => {
        const {WorkersActions} = this.props;
        WorkersActions.onDelete(id)
    };
    addOffice = (city: any) => {
        const {OfficesActions} = this.props;
        OfficesActions.onAdd({
           id: this.props.offices.length + 1,
           city,
           workers: [],
           orgId: this.state.currentOrganizationId,
        });
    };
    switchTab = (id: number) => {
        this.setState({
            activeTab: id
        });
    };
    render() {
        if (!this.props.loginStatus) return (<Redirect to="/auth" />);
        let currentOrganization = this.props.organizations[this.state.currentOrganizationId];
        let currentOffice = this.props.offices[this.state.currentWorkersId];
        return (
        <div>
            <h5>Здравствуйте, { this.props.login }</h5>
            <ul className="tabs">
                {this.state.activeTab <= 2 && this.state.activeTab >= 0 && <li className={this.state.activeTab === 0 && 'active'} onClick={() => this.switchTab(0)}>Организации</li>}
                {this.state.activeTab <= 2 && this.state.activeTab >= 1 && <li className={this.state.activeTab === 1 && 'active'} onClick={() => this.switchTab(1)}>Офисы</li>}
                {this.state.activeTab === 2&&<li className={this.state.activeTab === 2 && 'active'} onClick={() => this.switchTab(2)}>Сотрудники</li>}
            </ul>
            {this.state.activeTab === 0&&<table className="table">
                <thead>
                    <tr>
                        <td>
                            <AddNewOrganization addOrganization={this.addOrganization}/>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    {
                        (this.props.organizations.length > 0) ? this.props.organizations.map((organization: any, index: any) => {
                            return (
                                <Organization key={index} organization={organization} deleteOrganization={this.deleteOrganization} changeOrganization={this.changeOrganization} />
                            )
                        }) : <tr><td>Организации не найдены</td></tr>
                    }
                </tbody>
            </table>}
            {currentOrganization&&
            this.state.activeTab === 1&&<div>
                <AddNewOffice addOffice={this.addOffice}/>
                <h3>Вы просматриваете оффисы компании {currentOrganization.name}</h3>
                <table className="table">
                    <thead>
                        <tr>
                            <td>
                                ID оффиса
                            </td>
                            <td>
                                Город
                            </td>
                            <td>
                                Показать сотрудников
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        //Какой тип может быть индексом?
                        this.props.offices.length > 0
                            ? this.props.offices.map((office: any, index: any) => {
                                return <Office changeWorkers = {this.changeWorkers} deleteOffice={this.deleteOffice} key={index} office={office}/>
                            })
                            : 'Нет оффисов'
                    }
                    </tbody>
                </table>
            </div>}
            {this.state.activeTab === 2&&<div>
                {currentOffice&&<h3>Вы просматриваете cтрудников оффиса {currentOffice.id}</h3>}
                <AddNewWorker addWorker={this.addWorker}/>
                <table className="table">
                    <thead>
                    <tr>
                        <td>
                            Имя
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        //Какой тип может быть индексом?
                        this.props.workers.length > 0 ?
                            this.props.workers.map((worker: any, index: any) => {
                            return <Worker updateWorker={this.updateWorker} deleteWorker={this.deleteWorker} key={index} worker={worker}/>
                        })
                            : 'Нет сотрудников'
                    }
                    </tbody>
                </table>
            </div>}
        </div>
        )
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loginStatus: state.user.loginStatus,
        loading: state.user.loading,
        login: state.user.login,
        organizations: state.organizations,
        offices: state.offices,
        workers: state.workers,
    };
}
function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch),
        OrganizationsActions: new OrganizationsActions(dispatch),
        OfficesActions: new OfficesActions(dispatch),
        WorkersActions: new WorkersActions(dispatch)
    }
}
const connectMain = connect(mapStateToProps, mapDispatchToProps)(Main);

export {connectMain as Main};
