import * as React from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {Dispatch} from 'redux';
import {IStoreState} from '../../models/StoreState'
import {IDispatchProps} from '../../models/DispatchProps'
import {Actions} from '../Actions/Actions';

interface IStateProps {
    loginStatus: boolean;
    loading: boolean;
    login: any;
}

//@todo почитай про ref и this.refs.refName.value
function onLoginChange(e: any) {
    this.setState({
        login: e.target.value
    });
}

function onPasswordChange(e: any) {
    this.setState({
        password: e.target.value
    });
}

class Auth extends React.Component<IStateProps & IDispatchProps, { login: string, password: string }>{
    constructor() {
        super();
        this.state = {
            login: '',
            password: '',
        }
    }
    //@todo вынести все в Action Creator
    handleLogin = () => {
        const {actions} = this.props;
        actions.onLogin();
    };
    render() {
        if (this.props.loginStatus)
            return <Redirect to="/" />;
        return (
            <div>
                <form>
                    <div className="form-group">
                        <label>Логин</label>
                        <input type="text" className="form-control" placeholder="Введите логин" onChange={onLoginChange.bind(this)}/>
                        <small id="emailHelp" className="form-text text-muted">Уникальное имя выданное вам при регистрации</small>
                    </div>
                    <div className="form-group">
                        <label>Пароль</label>
                        <input type="password" className="form-control" placeholder="Введите пароль" onChange={onPasswordChange.bind(this)} />
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" />
                        <label className="form-check-label" >Согласен на использование сервисом cookies.</label>
                    </div>
                    <input className="btn btn-primary" type="button" value="Авторизироваться" onClick={this.handleLogin}/>
                </form>
            </div>
        )
    }
}
function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loginStatus: state.user.loginStatus,
        loading: state.user.loading,
        login: state.user.login,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    }
}
const connectAuth = connect(mapStateToProps, mapDispatchToProps)(Auth);
export {connectAuth as Auth};
