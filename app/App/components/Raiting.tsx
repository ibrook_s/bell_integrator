import * as React from 'react';
class Raiting extends React.Component<{raiting: any}, {}>{
    render() {
        const raiting = this.props.raiting;
        let width;
        width = raiting * 20 + '%';
        let divStyle = {
            width: width,
            background: '#d83d31',
            height: '100%',
            color: '#fff',
            textAlign: 'center'
        };
        return (
            <div className="raiting" style={divStyle}>{raiting * 100 / 5}%</div>
        )
    }
}
export default Raiting;