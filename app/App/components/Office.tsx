import * as React from 'react';
class Office extends React.Component<{office: any, changeWorkers: any, deleteOffice: any}, {showWorkers: boolean}>{
    constructor () {
        super();
        this.state = {
            showWorkers: false
        }
    }
    render() {
        const { office, deleteOffice } = this.props;

        return (
            <tr>
                <td>{office.id}</td>
                <td>{office.city}</td>
                <td><input type="button" className="btn btn-info" data-toggle="button"
                           onClick={() => deleteOffice(office.id)} value="Удалить" /></td>
                <td><input type="button" className="btn btn-info" data-toggle="button"
                           onClick={() => this.props.changeWorkers(office.id - 1)} value="Показать сотрудников"/></td>
            </tr>
        )
    }
}
export default Office;
