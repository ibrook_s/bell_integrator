import * as React from 'react';
class AddNewWorker extends React.Component<{addWorker: any}, {name: String, id: Number}> {
    constructor () {
        super();
        this.state = {
            name: '',
            id: 0,
        }
    };
    changeName = (e: any) => {
        this.setState({
            name: e.target.value
        })
    };
    render() {
        return (
            <form>
                <div className="form-group">
                    <label>Имя сотрудника</label>
                    <input type="text" className="form-control" onChange={this.changeName} placeholder="Имя сотрудника" />
                </div>
                <div className="form-group">
                    <input type="button" className="btn btn-info" value="Добавить" onClick={() => this.props.addWorker(this.state.name)}/>
                </div>
            </form>
        )
    }
}
export default AddNewWorker
