import * as React from 'react';
class AddNewOffice extends React.Component<{addOffice: any}, {id?: any, city?: any}> {
    constructor () {
        super();
        this.state = {
            id: '',
            city: '',
        }
    }
    changeCity = (e: any) => {
        this.setState({
            city: e.target.value
        })
    };
    render() {
        return (
            <form>
                <div className="form-group">
                    <label>Город</label>
                    <input type="text" className="form-control" onChange={this.changeCity} placeholder="Название города"/>
                </div>
                <div className="form-group">
                    <input type="button" className="btn btn-info" value="Добавить" onClick={() => this.props.addOffice(this.state.city)}/>
                </div>
            </form>
        )
    }
}
export default AddNewOffice
