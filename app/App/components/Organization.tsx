import * as React from 'react';
// import Raiting from './Raiting';
class Organization extends React.Component<{organization: any, changeOrganization: any, deleteOrganization: any}, {}>{
    render() {
        const organization = this.props.organization;
        const changeOrganization = this.props.changeOrganization;
        const deleteOrganization = this.props.deleteOrganization;
        return (
            <tr>
                <td><img src={organization.logo} height="40px" /></td>
                <td>{organization.name}</td>
                {/*<td style={raitingStyle}>*/}
                    {/*<Raiting raiting = {organization.raiting}/>*/}
                {/*</td>*/}
                {/*Временное решение с id*/}
                <td>
                    <input type="button" className="btn btn-info" data-toggle="button" value="Показать оффисы"
                           onClick={() => changeOrganization(organization.id - 1)} />
                </td>
                <td>
                    <input type="button" className="btn btn-info" data-toggle="button"
                           onClick={() => deleteOrganization(organization.id)} value="Удалить" />
                </td>
            </tr>
        )
    }
}
export default Organization
