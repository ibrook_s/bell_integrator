import * as React from 'react';
class Worker extends React.Component<{worker: any, deleteWorker: any, updateWorker: any}, { isEditing: boolean, name: String}>{
    constructor() {
        super();
        this.state = {
            isEditing: false,
            name: ''
        }
    }
    edit = () => {
        const { worker, updateWorker } = this.props;
        this.state.isEditing && updateWorker(worker.id, this.state.name);
        this.setState({
            isEditing: !this.state.isEditing,
        });
    };
    updateName = (e: any) => {
        this.setState({
            name: e.target.value
        })
    };
    render() {
        const { worker, deleteWorker } = this.props;
        return (
            <tr>
                {!this.state.isEditing
                    ? <td>{worker.name}</td>
                    : <td><input type="text" onInput={this.updateName} placeholder={worker.name}/></td>
                }
                <td><input type="button" className="btn btn-info" data-toggle="button" onClick={() => deleteWorker(worker.id)} value="Удалить"/></td>
                <td><input type="button" className="btn btn-info" data-toggle="button" onClick={this.edit}
                    value = {
                        !this.state.isEditing
                            ? 'Редактировать'
                            : 'Сохранить'
                    } />
                </td>
            </tr>
        )
    }
}
export default Worker;
