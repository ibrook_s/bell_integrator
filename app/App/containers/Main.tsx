import * as React from 'react';
import { Main } from '../components/Main';

class MainContiner extends React.Component {

    render() {
        return (
            <div className="padding">
                <h3>Система управления организациями</h3>
                <Main></Main>
            </div>
        )
    }
}

export { MainContiner as MainPage }
