import * as React from 'react';
import { Auth } from '../components/Auth';

class AuthContainer extends React.Component {

    render() {
        return (
            <div className="padding">
                <h4>Авторизация</h4>
                <Auth></Auth>
            </div>
        )
    }
}

export { AuthContainer as AuthPage }
