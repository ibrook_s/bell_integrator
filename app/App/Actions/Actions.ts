import {Dispatch} from 'redux';
import {ActionTypes, AsyncActionTypes} from '../Actions/ActionsConsts';
import {IDispatchProps} from '../../models/DispatchProps';
export class Actions {
  constructor(private dispatch: Dispatch<IDispatchProps>) {
  }

  onClick = (i: number) => this.dispatch({type: ActionTypes.CLICK, payload: i});

  onLogin = () => {
     fetch('http://www.mocky.io/v2/5aaadd8d330000bd082daba8')
        .then((data) => {
        this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`});
            return data.json();
        })
        .then((user) => {
            this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
                dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`, payload: user.login});
            });
        })
        .catch((err) => {
            this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAIL}`, payload: err});
        });
  };

  onLogout = () => this.dispatch({type: ActionTypes.LOGOUT});

  onLoadOrganizations = () => {
      fetch('http://www.mocky.io/v2/5aca6b892e00005200bba9d7')
          .then((data) => {
            return data.json();
          })
          .then((organizations) => {
              this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
                  dispatch({type: `${ActionTypes.LOAD_ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`, payload: organizations});
              });
          })
          .catch((err) => {
              alert(err);
          })
  };
  onLoadOffices = (orgId: Number) => {
      fetch('http://www.mocky.io/v2/5aca68b62e00004900bba9d3')
          .then((data) => {
              return data.json();
          })
          .then((offices) => {
              this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
                  dispatch({type: `${ActionTypes.LOAD_OFFICES}${AsyncActionTypes.SUCCESS}`, payload: offices.filter((office: any) => orgId === office.orgId)});
              });
          })
          .catch((err) => {
              alert(err);
          });
  };
  onLoadWorkers = (offId: Number) => {
      fetch('http://www.mocky.io/v2/5aca694c2e00006100bba9d4')
          .then((data) => {
              return data.json();
          })
          .then((workers) => {
              this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
                  dispatch({type: `${ActionTypes.LOAD_WORKERS}${AsyncActionTypes.SUCCESS}`, payload: workers.filter((worker: any) => offId === worker.offId)});
              });
          })
          .catch((err) => {
              alert(err);
          });
  };
  onAddOrganization = (payload: any) => {
      this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
          dispatch({type: ActionTypes.ADD_ORGANIZATION, payload})
      });
  }
}
