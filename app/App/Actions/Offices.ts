import {Dispatch} from 'redux';
import {ActionTypes, /* AsyncActionTypes */} from '../Actions/ActionsConsts';

export interface IDispatchProps {
    OfficesActions: OfficesActions;
}

export class OfficesActions {
    constructor(private dispatch: Dispatch<IDispatchProps>) {
    }
    onAdd = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.ADD_OFFICE, payload})
        });
    };
    onDelete = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.DELETE_OFFICE, payload})
        });
    }
}
