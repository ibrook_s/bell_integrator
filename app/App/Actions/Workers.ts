import {Dispatch} from 'redux';
import {ActionTypes, /* AsyncActionTypes */} from '../Actions/ActionsConsts';

export interface IDispatchProps {
    WorkersActions: WorkersActions;
}

export class WorkersActions {
    constructor(private dispatch: Dispatch<IDispatchProps>) {
    }
    onAdd = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.ADD_WORKER, payload})
        });
    };
    onUpdate = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.UPDATE_WORKER, payload})
        });
    };
    onDelete = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.DELETE_WORKER, payload})
        });
    }
}
