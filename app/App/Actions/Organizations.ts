import {Dispatch} from 'redux';
import {ActionTypes, /* AsyncActionTypes */} from '../Actions/ActionsConsts';

export interface IDispatchProps {
    OrganizationsActions: OrganizationsActions;
}

export class OrganizationsActions {
    constructor(private dispatch: Dispatch<IDispatchProps>) {
    }
    onAdd = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.ADD_ORGANIZATION, payload})
        });
    };
    onDelete = (payload: any) => {
        this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
            dispatch({type: ActionTypes.DELETE_ORGANIZATION, payload})
        });
    }
}
