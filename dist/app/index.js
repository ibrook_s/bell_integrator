"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var react_redux_1 = require("react-redux");
var react_router_dom_1 = require("react-router-dom");
var Auth_1 = require("./App/containers/Auth");
var Main_1 = require("./App/containers/Main");
var Store_1 = require("./Store/Store");
require("./App/styles/style.less");
ReactDOM.render(React.createElement(react_redux_1.Provider, { store: Store_1.appStore },
    React.createElement("div", { className: "container" },
        React.createElement(react_router_dom_1.BrowserRouter, null,
            React.createElement(react_router_dom_1.Switch, null,
                React.createElement(react_router_dom_1.Route, { exact: true, path: "/", component: Main_1.MainPage }),
                React.createElement(react_router_dom_1.Route, { path: "/auth", component: Auth_1.AuthPage }))))), document.getElementById('app'));
//# sourceMappingURL=index.js.map