"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var redux_1 = require("redux");
var redux_2 = require("redux");
var redux_devtools_extension_1 = require("redux-devtools-extension");
var redux_thunk_1 = require("redux-thunk");
var ActionsConsts_1 = require("../App/Actions/ActionsConsts");
var initialState = {
    get state() {
        return {
            loginStatus: false,
            loading: false,
        };
    }
};
function user(state, action) {
    if (state === void 0) { state = initialState.state; }
    switch (action.type) {
        case "" + ActionsConsts_1.ActionTypes.LOGIN + ActionsConsts_1.AsyncActionTypes.BEGIN:
            return __assign({}, state, { loading: true });
        case "" + ActionsConsts_1.ActionTypes.LOGIN + ActionsConsts_1.AsyncActionTypes.SUCCESS:
            return __assign({}, state, { loginStatus: true, loading: false, login: action.payload });
        case "" + ActionsConsts_1.ActionTypes.LOGIN + ActionsConsts_1.AsyncActionTypes.FAIL:
            return __assign({}, state, { loading: false, loginStatus: false });
        case ActionsConsts_1.ActionTypes.LOGOUT:
            return __assign({}, state, { loginStatus: false });
    }
    return state;
}
function organizations(state, action) {
    if (state === void 0) { state = initialState.state; }
    switch (action.type) {
        case "" + ActionsConsts_1.ActionTypes.LOAD + ActionsConsts_1.AsyncActionTypes.SUCCESS:
            return action.payload;
        case "" + ActionsConsts_1.ActionTypes.ADD_ORGANIZATION:
            return state.concat([
                action.payload
            ]);
        case "" + ActionsConsts_1.ActionTypes.DELETE_ORGANIZATION:
            console.log(state, action.payload);
            return state.filter(function (organization) {
                return organization.id !== action.payload;
            });
    }
    return state;
}
var reducers = redux_2.combineReducers({
    organizations: organizations,
    user: user
});
var store = redux_1.createStore(reducers, redux_devtools_extension_1.composeWithDevTools(redux_1.applyMiddleware(redux_thunk_1.default)));
exports.appStore = store;
//# sourceMappingURL=Store.js.map