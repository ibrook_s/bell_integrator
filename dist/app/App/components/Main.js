"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var react_router_dom_1 = require("react-router-dom");
var Actions_1 = require("../Actions/Actions");
var Organizations_1 = require("../Actions/Organizations");
var AddNewOrganization_1 = require("./AddNewOrganization");
var Office_1 = require("./Office");
var Organization_1 = require("./Organization");
var Worker_1 = require("./Worker");
var Main = /** @class */ (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = _super.call(this) || this;
        _this.changeOrganization = function (id) {
            _this.setState({
                currentOrganizationId: id,
                currentWorkersId: 0
            });
        };
        _this.changeWorkers = function (id) {
            _this.setState({
                currentWorkersId: id
            });
        };
        _this.loadOrganizations = function () {
            var actions = _this.props.actions;
            actions.onLoadOrganizations();
        };
        _this.addOrganization = function (name, logoUrl) {
            var OrganizationsActions = _this.props.OrganizationsActions;
            OrganizationsActions.onAdd({
                id: (new Date()).getTime(),
                name: name,
                raiting: '1',
                logo: logoUrl,
                offices: []
            });
        };
        _this.showAddOrganization = function () {
            _this.setState({
                showAddOrganization: !_this.state.showAddOrganization
            });
        };
        _this.deleteOrganization = function (id) {
            var OrganizationsActions = _this.props.OrganizationsActions;
            OrganizationsActions.onDelete(id);
        };
        _this.state = {
            currentOrganizationId: 0,
            currentWorkersId: 0,
            showAddOrganization: false,
        };
        return _this;
    }
    Main.prototype.componentWillMount = function () {
        this.loadOrganizations();
    };
    Main.prototype.render = function () {
        var _this = this;
        if (!this.props.loginStatus)
            return (React.createElement(react_router_dom_1.Redirect, { to: "/auth" }));
        var currentOrganization = this.props.organizations[this.state.currentOrganizationId];
        var currentOffice = currentOrganization && currentOrganization.offices[this.state.currentWorkersId];
        return (React.createElement("div", null,
            React.createElement("h5", null,
                "\u0417\u0434\u0440\u0430\u0432\u0441\u0442\u0432\u0443\u0439\u0442\u0435, ",
                this.props.login),
            React.createElement("button", { onClick: this.showAddOrganization }, !this.state.showAddOrganization
                ? 'Отображать добавление организации'
                : 'Скрыть добавление организации'),
            this.state.showAddOrganization && React.createElement(AddNewOrganization_1.default, { addOrganization: this.addOrganization }),
            React.createElement("table", { className: "table" },
                React.createElement("tbody", null, (this.props.organizations.length > 0) ? this.props.organizations.map(function (organization, index) {
                    return (React.createElement(Organization_1.default, { key: index, organization: organization, deleteOrganization: _this.deleteOrganization, changeOrganization: _this.changeOrganization }));
                }) : React.createElement("h1", null, "\u041E\u0440\u0433\u0430\u043D\u0438\u0437\u0430\u0446\u0438\u0438 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B"))),
            currentOrganization &&
                React.createElement("div", null,
                    React.createElement("h3", null,
                        "\u0412\u044B \u043F\u0440\u043E\u0441\u043C\u0430\u0442\u0440\u0438\u0432\u0430\u0435\u0442\u0435 \u043E\u0444\u0444\u0438\u0441\u044B \u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0438 ",
                        currentOrganization.name),
                    React.createElement("table", { className: "table" },
                        React.createElement("thead", null,
                            React.createElement("tr", null,
                                React.createElement("td", null, "ID \u043E\u0444\u0444\u0438\u0441\u0430"),
                                React.createElement("td", null, "\u0413\u043E\u0440\u043E\u0434"),
                                React.createElement("td", null, "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0441\u043E\u0442\u0440\u0443\u0434\u043D\u0438\u043A\u043E\u0432"))),
                        React.createElement("tbody", null, 
                        //Какой тип может быть индексом?
                        this.props.organizations[this.state.currentOrganizationId].offices.map(function (office, index) {
                            return React.createElement(Office_1.default, { changeWorkers: _this.changeWorkers, key: index, office: office });
                        }))),
                    React.createElement("h3", null,
                        "\u0412\u044B \u043F\u0440\u043E\u0441\u043C\u0430\u0442\u0440\u0438\u0432\u0430\u0435\u0442\u0435 c\u0442\u0440\u0443\u0434\u043D\u0438\u043A\u043E\u0432 \u043E\u0444\u0444\u0438\u0441\u0430 ",
                        currentOffice.id),
                    React.createElement("table", { className: "table" },
                        React.createElement("thead", null,
                            React.createElement("tr", null,
                                React.createElement("td", null, "\u0418\u043C\u044F"))),
                        React.createElement("tbody", null, 
                        //Какой тип может быть индексом?
                        currentOffice.workers.map(function (worker, index) {
                            return React.createElement(Worker_1.default, { key: index, worker: worker });
                        }))))));
    };
    return Main;
}(React.Component));
function mapStateToProps(state) {
    return {
        loginStatus: state.user.loginStatus,
        loading: state.user.loading,
        login: state.user.login,
        organizations: state.organizations
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: new Actions_1.Actions(dispatch),
        OrganizationsActions: new Organizations_1.OrganizationsActions(dispatch)
    };
}
var connectMain = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(Main);
exports.Main = connectMain;
//# sourceMappingURL=Main.js.map