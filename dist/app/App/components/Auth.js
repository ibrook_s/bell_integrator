"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_redux_1 = require("react-redux");
var react_router_dom_1 = require("react-router-dom");
var Actions_1 = require("../Actions/Actions");
//@todo почитай про ref и this.refs.refName.value
function onLoginChange(e) {
    this.setState({
        login: e.target.value
    });
}
function onPasswordChange(e) {
    this.setState({
        password: e.target.value
    });
}
var Auth = /** @class */ (function (_super) {
    __extends(Auth, _super);
    function Auth() {
        var _this = _super.call(this) || this;
        //@todo вынести все в Action Creator
        _this.handleLogin = function () {
            var actions = _this.props.actions;
            actions.onLogin();
        };
        _this.state = {
            login: '',
            password: '',
        };
        return _this;
    }
    Auth.prototype.render = function () {
        if (this.props.loginStatus)
            return React.createElement(react_router_dom_1.Redirect, { to: "/" });
        return (React.createElement("div", null,
            React.createElement("form", null,
                React.createElement("div", { className: "form-group" },
                    React.createElement("label", null, "\u041B\u043E\u0433\u0438\u043D"),
                    React.createElement("input", { type: "text", className: "form-control", placeholder: "\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043B\u043E\u0433\u0438\u043D", onChange: onLoginChange.bind(this) }),
                    React.createElement("small", { id: "emailHelp", className: "form-text text-muted" }, "\u0423\u043D\u0438\u043A\u0430\u043B\u044C\u043D\u043E\u0435 \u0438\u043C\u044F \u0432\u044B\u0434\u0430\u043D\u043D\u043E\u0435 \u0432\u0430\u043C \u043F\u0440\u0438 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438")),
                React.createElement("div", { className: "form-group" },
                    React.createElement("label", null, "\u041F\u0430\u0440\u043E\u043B\u044C"),
                    React.createElement("input", { type: "password", className: "form-control", placeholder: "\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C", onChange: onPasswordChange.bind(this) })),
                React.createElement("div", { className: "form-check" },
                    React.createElement("input", { type: "checkbox", className: "form-check-input" }),
                    React.createElement("label", { className: "form-check-label" }, "\u0421\u043E\u0433\u043B\u0430\u0441\u0435\u043D \u043D\u0430 \u0438\u0441\u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u043D\u0438\u0435 \u0441\u0435\u0440\u0432\u0438\u0441\u043E\u043C cookies.")),
                React.createElement("input", { className: "btn btn-primary", type: "button", value: "\u0410\u0432\u0442\u043E\u0440\u0438\u0437\u0438\u0440\u043E\u0432\u0430\u0442\u044C\u0441\u044F", onClick: this.handleLogin }))));
    };
    return Auth;
}(React.Component));
function mapStateToProps(state) {
    return {
        loginStatus: state.user.loginStatus,
        loading: state.user.loading,
        login: state.user.login,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: new Actions_1.Actions(dispatch)
    };
}
var connectAuth = react_redux_1.connect(mapStateToProps, mapDispatchToProps)(Auth);
exports.Auth = connectAuth;
//# sourceMappingURL=Auth.js.map