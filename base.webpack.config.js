"use strict";

const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    context: path.resolve(__dirname, "./app"),
    entry: {
        app: "./index.tsx",
        vendors: "./App/vendors/css.ts"
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            enforce: "pre",
            use: "tslint-loader"
        }, {
            test: /\.tsx?$/,
            use: "ts-loader"
        }, {
            test: /\.(css)$/,
            use: [{
                loader: 'style-loader',
            }, {
                loader: 'css-loader',
            }]
        }, {
        test: /\.(scss)$/,
        use: [{
            loader: 'style-loader',
        }, {
            loader: 'css-loader',
        }, {
            loader: 'postcss-loader',
            options: {
                plugins: function () {
                    return [
                        require('precss'),
                        require('autoprefixer')
                    ];
                }
            }
        }, {
            loader: 'sass-loader'
        }]
        }, {
            test: /\.less$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: ["css-loader", "less-loader"]
            })
        }]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/app/index.html"
        }),
        new ExtractTextPlugin("style.css"),
        new webpack.ProvidePlugin({
            Promise: "imports-loader?this=>global!exports-loader?global.Promise!bluebird"
        }),
    ]
};